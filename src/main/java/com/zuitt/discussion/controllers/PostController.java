package com.zuitt.discussion.controllers;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.PostRepository;
import com.zuitt.discussion.repositories.UserRepository;
import com.zuitt.discussion.services.PostService;
import com.zuitt.discussion.services.PostServiceImpl;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
//fixes cors error enables cross origin request via @CrossOrigin.
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;



//    @GetMapping("/getPost/{id}")
//    @ResponseBody
//    public Optional<Post> findById(@PathVariable(name="id")long id){
//        return postRepository.findById(id);
//
//    }


    @PostMapping("/posts")
    public ResponseEntity<Object>createPost(@RequestHeader(value = "Authorization") String stringToken,@RequestBody Post post){
        //Retrieve the "user" object using the extracted username from jwt token.

        postService.createPost(stringToken,post);

        return new ResponseEntity<>("Posts created successfully" ,HttpStatus.CREATED);
    }
    @GetMapping("/posts")
    public ResponseEntity<Object>createPost(){
        return new ResponseEntity<>(postService.getPosts(),HttpStatus.OK);
    }
    @DeleteMapping("/posts/{postid}")
    public ResponseEntity<Object>deletePost(@PathVariable(name="postid")Long postid){
        return postService.deletePost(postid);
    }
    @PutMapping("/posts/{postid}")
    public ResponseEntity<Object>updatePost(@PathVariable(name="postid")Long postid,@RequestBody Post post){
        return postService.updatePost(postid, post);
    }






}