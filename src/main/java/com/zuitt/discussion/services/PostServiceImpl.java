package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.PostRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

//allows us to use crud repository methods
@Service
public class PostServiceImpl implements PostService {

    //@autowired allows us to use an inteface as if it was an instance of an object
    @Autowired
    UserRepository userRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    JwtToken jwtToken;
    public void createPost(String stringToken,Post post) {

        User author= userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost =new Post();
        newPost.setUser(author);
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        postRepository.save(newPost);
    }
    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    @Override
    public ResponseEntity deletePost(Long id) {
        postRepository.deleteById(id);

        return new ResponseEntity<>("Post Deleted Succesfully", HttpStatus.OK);
    }

    @Override
    public ResponseEntity updatePost(Long id,Post post) {
        Post postToUpdate=postRepository.findById(id).get();
        postToUpdate.setContent(post.getContent());
        postToUpdate.setTitle(post.getTitle());

        postRepository.save(postToUpdate);
        return new ResponseEntity<>("Succesfully updated",HttpStatus.OK);
    }


}
